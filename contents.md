# Talks

## Python User Group

### Lightning Talks

* [Upgrade your Python with Async](upgrade-your-python-with-async.md)
* [Prosocial Intuition Pumps](prosocial-intuition-pumps.md)
* [Laundry Room Smiley](laundry-room-smiley.md)
* [Data Science Playground](https://gitlab.com/tangibleai/team/-/tree/0-play/)
* [Laundry List of Prosocial Ideas](laundry-list.md)
* [Django Channels for Chatbots](django-channels-for-chatbots.md)
* [Kinds of Intuition Pumps](kinds-of-intuition.md)
* [Prosocial Pumps](prosocial-pumps.md)
* [Squashing Hyperspace with a Twitter Bot](Squashing-Hyperspace-with-a-Twitter-Bot.pdf)
* [README.md](README.md)