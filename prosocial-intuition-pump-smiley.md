# Social Intuition Pumps for Engineers
## What is an Intuition Pump?

**_Intuition Pumps_**
_and Tools for Thinking_


## What is an Intuition Pump?
:::{note}
_Intuition Pumps and Tools for Thinking_
:::

* by Daniel C. Dennett

## Prosocial?

Cooperation beats competition in the game of life.

## Make Change

![Change bowl for laundry room with quarters and dollars and smiley face on lid](images/laundry-change-smile.jpg)