# Prosocial Intuition Pumps

## What's an Intuition Pump

:::{note}
_Intuition Pumps and Tools for Thinking_
-- Daniel C. Dennett
:::

77 _Tools for Thinking_!!!

## Kinds

* II. General
* III. Meaning & Content
* IV. Computers
* V. More Meaning
* VI. Evolution
* VII. Consciousness
* VIII. Free Will
* IX. Philosophy

## Examples of Intuition Pumps

### 1. Make Mistakes
### 4. Sturgeon's Law 
### 5. Occam's Razor

## Examples of Prosocial Pumps

### 101. Laundry room smilie


