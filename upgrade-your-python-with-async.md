# _Upgrade_ your Python with coroutines and `async`

# Resources

- Real Python tutorial: [proai.org / rp - async](https: // realpython.com / async-io - python/)
- [docs.python.org](https: // docs.python.org / 3.8 / library / asyncio - task.html  # coroutine)
- [How to use all 3 together](https: // stackoverflow.com / a / 70920890 / 623735)






----









# Multitasking brains in _Upgrade_

# Fork vs Spawn vs Async

- Fork: CPU - bound tasks
- Spawn: CPU - bound tasks
- Multithreading: IO - bound tasks




----







# Fork (`os.fork`)

Creates a new branch in the abstract syntax tree and continues along both branches as if nothing happened.

- FAST(to create the fork)
- unsafe
- bloated

WARNING: DON'T FORK THREADED PROCESSES! (async await etc)






----








# Spawn (multiprocessing):

Create new python interpreter from scratch

- SLOW(to spawn a process)
- Run code on multiple CPUs in parallel
- Inherits everything in RAM except file descriptors and handles






-----









# Multithreading w/ `async` & `await` (`asyncio`)

Coroutines that return something sometime in the future.

- Great for IO Bound tasks(disk / network)
- You know when / where you want to switch between threads




-----






# Async


```python
import asyncio

async def main():
    print('Hello ...')
    await asyncio.sleep(1)
    print('... World!')

asyncio.run(main())
```




-----




```python
async def main():
    task1 = asyncio.create_task(
        say_after(1, 'hello'))

    task2 = asyncio.create_task(
        say_after(2, 'world'))

    print(f"started at {time.strftime('%X')}")

    # Wait until both tasks are completed (should take
    # around 2 seconds.)
    await task1
    await task2

    print(f"finished at {time.strftime('%X')}")
```




----



```python
import time

async def say_after(delay, what):
    await asyncio.sleep(delay)
    print(what)

async def main():
    print(f"Clocking in at {time.strftime('%X')}")

    await say_after(1, f"Starting 1 sec job at {time.strftime('%X')}")
    await say_after(2, f"Starting 2 sec job at {time.strftime('%X')}")
    

    print(f"All DONE at {time.strftime('%X')}")

asyncio.run(main())
```

# References



# Async (threads)

# What is async

-

# What is a coroutine

-
