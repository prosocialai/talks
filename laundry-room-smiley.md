# Make Change

"Studies have shown" that putting a smilie face on tip jars and honor bar cash boxes will increase the positive sum.

Experiment with care... it may have unintended consequences:

![Change bowl for laundry room with quarters and dollars and smiley face on lid](images/laundry-room-smilie.jpg)

- staff moved the smiley change bowl and detergent to a 7 ft high cabinet
- the $5 was still there months later
- the quarters seemed undisturbed
- the coin slot got jammed and stopped working 
- found a foreign coin in the change bowl (my fault)
- almost daily I removed bricks that were used to prop open the gate
- a month after the coin slot got jammed, vandals bashed it off with a brick
- staff started locking up the laundry at night
- staff added security cameras outside the door
- staff keeps bricks away from gate
- the bricks eventually stopped appearing
- one of the new machines broke down
- no further vandalism
- change box continues to "work" (out of reach/site/mind)
