# Build Better Bots

### With Agile Machine Learning

<!-- .slide: data-transition="zoom" -->

---

## The Basics

- function = mapping = model
- classify = tag = understand (NLU)
- labeling data to train a bot
- machine-assisted labeling

---

## Math testing

  $f(x) = m \cdot x + b$

```
f(x) == m * x + b
```

---

## What is a function?

```python
def f(x):
    m = 2
    b = 3
    y = m * x + b
    return y
```

Note:
- Who recognize this function?
- In algebra class you learned about the function for a line

---

## More markdown (fragments)

* static text
* fragment <!-- .element: class="fragment" -->
* fragment grow <!-- .element: class="fragment grow" -->
* fragment highlight-red <!-- .element: class="fragment highlight-red" -->
* press key down <!-- .element: class="fragment fade-up" -->

--

## More markdown (tables)

****

|h1|h2|h3|
|-|-|-|
|a|b|c|

****

--

## More markdown (code)

```
version: '2'
services:
  slides:
    image: msoedov/hacker-slides

    ports:
      - 8080:8080
    volumes:
      - ./slides:/app/slides
    restart: always

    environment:
     - USER=bob
     - PASSWORD=pa55

```

--

## Local images

![demoPicture](/images/demo.png)

Copy images into slides/images/ & include with MD:

```
![demoPicture](/images/demo.png)

```
or HTML:

```
<img src="/images/demo.png">

```


---

## Learn more

- [RevealJS Demo/Manual](http://lab.hakim.se/reveal-js)
- [RevealJS Project/README](https://github.com/hakimel/reveal.js)
- [GitHub Flavored Markdown](https://help.github.com/articles/github-flavored-markdown)
