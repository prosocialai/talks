# Django Channels for Chatbots

- `async` and `await`
- `http://` vs _WebSockets_ ( `ws://` )
- channels and groups
- install django-channels
- install aiohttp
- switch to asgi.py
- add channels to INSTALLED_APPS
- create consumer
- demo

## HTTP (REST) Network Diagram

[http://](images/django-http-requests-chatbot-page-1.drawio.png)

## Websockets (Channels) Network Diagram

[ws://](images/django-http-requests-chatbot-page-2.drawio.png)
